package com.cadastrodepessoa.persistencia;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cadastrodepessoa.dominio.Pessoa;
import com.cadastrodepessoa.dominio.Pessoa.Pessoas;

public class CadastroDePessoa {

	private BancoHelper helper;

	public CadastroDePessoa(Context context) {
		helper = new BancoHelper(context);
	}

	public void incluir(Pessoa pessoa) {

		ContentValues valores = new ContentValues();
		valores.put(Pessoas.NOME, pessoa.getNome());
		valores.put(Pessoas.SOBRENOME, pessoa.getSobrenome());

		SQLiteDatabase db = helper.getWritableDatabase();
		db.insert(Pessoas.PESSOA, null, valores);
		db.close();
	}

	public ArrayList<Pessoa> listarPessoas(String nome, String sobrenome) {

		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		Cursor resultado = obterCursor(nome, sobrenome);
		
		while (resultado.moveToNext()){
			Pessoa pessoa = new Pessoa();
			
			int id = resultado.getInt(resultado.getColumnIndex(Pessoas.ID));
			String n = resultado.getString(resultado.getColumnIndex(Pessoas.NOME));
			String s = resultado.getString(resultado.getColumnIndex(Pessoas.SOBRENOME));
			
			pessoa.setId(id);
			pessoa.setNome(n);
			pessoa.setSobrenome(s);
			//byte[] foto = resultado.getBlob(resultado.getColumnIndex(Pessoa.FOTO));
			
			pessoas.add(pessoa);
			
		}
		

		return pessoas;
	}

	public Cursor obterCursor(String nome, String sobrenome) {

		String[] colunas = { Pessoas.ID, Pessoas.NOME, Pessoas.SOBRENOME };
		SQLiteDatabase db = helper.getReadableDatabase();
		Cursor resultado;
		if (!nome.isEmpty() && !sobrenome.isEmpty()) {
			String condicao = Pessoas.NOME + " like ? and " + Pessoas.SOBRENOME
					+ " like ? ";
			String[] argumentos = { nome, sobrenome };
			resultado = db.query(Pessoas.PESSOA, colunas, condicao, argumentos,
					null, null, null);
		} else if (!nome.isEmpty()) {
			String condicao = Pessoas.NOME + " like ? ";
			String[] argumentos = { nome };
			resultado = db.query(Pessoas.PESSOA, colunas, condicao, argumentos,
					null, null, null);
		} else if (!sobrenome.isEmpty()) {
			String condicao = Pessoas.SOBRENOME + " like ? ";
			String[] argumentos = { sobrenome };
			resultado = db.query(Pessoas.PESSOA, colunas, condicao, argumentos,
					null, null, null);
		} else {
			resultado = db.query(Pessoas.PESSOA, colunas, null, null, null,
					null, null);
		}
		return resultado;
	}

	public void alterar(Pessoa pessoa) {
		//Implementar a lógica de alteração.		
	}

}
